﻿using System;
using UnityEngine;

public class Bounds : MonoBehaviour
{
    public Action OnBallOut;
    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Ball"))
        {
            OnBallOut?.Invoke();
        }
    }
}
