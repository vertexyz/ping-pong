﻿using Lean.Touch;
using UnityEngine;

public class Racket : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private LeanMultiSet leanMultiSet;
    
    private GameSettings _gameSettings;
    
    private Vector2 _size;
    
    private void Awake()
    {
        _gameSettings = GameSettingsService.Get().GameSettings;
    
        _size = spriteRenderer.size;

        leanMultiSet.Multiplier = _gameSettings.racketMoveMultiplier;
    }

    public void Translate(Vector2 delta)
    {
        transform.Translate(Vector3.right * delta.x / _gameSettings.racketTranslateFactor);

        var position = transform.localPosition;
        position.x = Mathf.Clamp(position.x, _gameSettings.horizontalBounds.x + _size.x / 2, _gameSettings.horizontalBounds.y - _size.x / 2);
        transform.localPosition = position;
    }
}
