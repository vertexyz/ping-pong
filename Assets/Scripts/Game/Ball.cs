﻿using System;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Action OnBounceFromRacket;

    [SerializeField]
    private SpriteRenderer spriteRenderer;
    
    [SerializeField]
    private CircleCollider2D circleCollider2D;
    
    [SerializeField]
    private Rigidbody2D rigidbody2D;

    private GameSettings _gameSettings;
    
    private int _yDirection;

    private float _speed;
    
    private void Awake()
    {
        _gameSettings = GameSettingsService.Get().GameSettings;
    }

    private void Start()
    {
        _speed = _gameSettings.ballSpeed + Utils.GetRandomRange(_gameSettings.ballSpeedRandomize);

        var size = _gameSettings.ballSize * Utils.GetRandomRange(_gameSettings.ballSizeRandomize);
        spriteRenderer.size = Vector2.one * size;
        circleCollider2D.radius = size / 2;
        
        _yDirection = Utils.GetRandomSign();

        rigidbody2D.velocity = Vector2.up * _yDirection * _speed;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Racket"))
        {
            var xOffsetFactor = GetBounceFactor(transform.position.x,
                col.transform.position.x,
                col.collider.bounds.size.x);

            _yDirection *= -1;

            var dir = new Vector2(xOffsetFactor, _yDirection).normalized;

            rigidbody2D.velocity = dir * _speed;
            
            OnBounceFromRacket?.Invoke();
        }
    }

    private float GetBounceFactor(float ballPosition, float racketPosition, float racketWidth)
    {
        return (ballPosition - racketPosition) / racketWidth * _gameSettings.ballBounceOffsetFactor +
               Utils.GetRandomRange(_gameSettings.ballBounceOffsetFactorRandomize);
    }

    public void SetColor(Color color)
    {
        spriteRenderer.color = color;
    }
}
