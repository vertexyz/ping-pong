﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoaderController : MonoBehaviour
{
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(2f);
        SceneLoaderService.Get().LoadScene(ScenesEnum.Lobby);
    }
}
