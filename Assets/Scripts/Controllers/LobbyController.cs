﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LobbyController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI hiScoreLabel;
    
    [SerializeField]
    private ColorPickerPanel colorPickerPanel;
    
    [SerializeField]
    private Image ballImage;

    private void Start()
    {
        colorPickerPanel.gameObject.SetActive(false);
        
        colorPickerPanel.OnColorChanged = OnColorChangedHandler;
        colorPickerPanel.OnClose = OnColorPickerPanelCloseHandler;
        
        ballImage.color = GameDataService.Get().PlayerColor;
        
        hiScoreLabel.text = "HiScore: " + GameDataService.Get().PlayerHiScore;
    }

    //Color Picker
    private void OnColorChangedHandler(Color color)
    {
        GameDataService.Get().PlayerColor = color;
        ballImage.color = color;
    }
    
    private void OnColorPickerPanelCloseHandler()
    {
        colorPickerPanel.gameObject.SetActive(false);
    }

    //Buttons handlers
    public void OnStartSinglePlayerGameButtonClick()
    {
        SceneLoaderService.Get().LoadScene(ScenesEnum.Game);
    }
    
    public void OnStartMultiPlayerGameButtonClick()
    {
        //TODO multiplayer game
    }
    
    public void OnOptionsButtonClick()
    {
        colorPickerPanel.gameObject.SetActive(true);
        colorPickerPanel.SetColor(GameDataService.Get().PlayerColor);
    }
    
    public void OnExitButtonClick()
    {
        Application.Quit();
    }
}
