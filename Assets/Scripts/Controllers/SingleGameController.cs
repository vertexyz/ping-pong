﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class SingleGameController : MonoBehaviour
{
    [SerializeField]
    private Transform spawnPoint;
    
    [SerializeField]
    private Ball ballPrefab;
    
    [SerializeField]
    private Bounds bounds;
    
    [SerializeField]
    private Racket racket_1;
    
    [SerializeField]
    private Racket racket_2;

    [SerializeField]
    private TextMeshProUGUI scoreLabel;
    
    [SerializeField]
    private TextMeshProUGUI hiscoreLabel;

    private GameSettings _gameSettings;
    
    private Ball ball;
    
    private void Awake()
    {
        _gameSettings = GameSettingsService.Get().GameSettings;
        bounds.OnBallOut = OnBallOutHandler;
    }
    
    private void Start()
    {
        StartGame();
    }

    private void StartGame()
    {
        GameDataService.Get().PlayerScore = 0;
        UpdateScoreLabels();
        CreateBall();
    }

    private void CreateBall()
    {
        ball = Instantiate(ballPrefab, spawnPoint);
        ball.OnBounceFromRacket = OnBounceFromRacketHandler;
        ball.SetColor(GameDataService.Get().PlayerColor);
    }

    private void OnBallOutHandler()
    {
        ball.OnBounceFromRacket = null;
        Destroy(ball.gameObject);

        StartCoroutine(DelayedeRestartGame());
    }

    private IEnumerator DelayedeRestartGame()
    {
        yield return new WaitForSeconds(1f);
        StartGame();
    }

    private void OnBounceFromRacketHandler()
    {
        GameDataService.Get().PlayerScore += _gameSettings.scorePerRacketHit;
        UpdateScoreLabels();
    }

    private void UpdateScoreLabels()
    {
        scoreLabel.text = "Score: " + GameDataService.Get().PlayerScore;
        hiscoreLabel.text = "HiScore: " + GameDataService.Get().PlayerHiScore;
    }

    public void OnGameExitButtonClick()
    {
        SceneLoaderService.Get().LoadScene(ScenesEnum.Lobby);
    }

    private void OnDestroy()
    {
        if (ball != null)
        {
            ball.OnBounceFromRacket = null;
        }

        bounds.OnBallOut = null;
    }
}
