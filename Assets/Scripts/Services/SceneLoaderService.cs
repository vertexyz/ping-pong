﻿using UnityEngine.SceneManagement;

public enum ScenesEnum
{
    Loader = 0,
    Lobby = 1,
    Game = 2
}

public class SceneLoaderService
{
    private static readonly SceneLoaderService _instance = new SceneLoaderService();

    public static SceneLoaderService Get()
    {
        return _instance;
    }

    public void LoadScene(ScenesEnum scene)
    {
        SceneManager.LoadScene((int)scene);
    }
    
    public ScenesEnum GetCurrentScene()
    {
        return (ScenesEnum)SceneManager.GetActiveScene().buildIndex;
    }
}
