﻿public class GameSettingsService
{
    private static readonly GameSettingsService _instance = new GameSettingsService();

    public static GameSettingsService Get()
    {
        return _instance;
    }

    private GameSettings _gameSetting;

    public GameSettings GameSettings
    {
        get => _gameSetting;
    }
    
    public void Init(GameSettings gameSettings)
    {
        _gameSetting = gameSettings;
    }
}
