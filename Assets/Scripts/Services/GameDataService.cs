﻿using UnityEngine;

public class GameDataService
{
    private static readonly GameDataService _instance = new GameDataService();

    public static GameDataService Get()
    {
        return _instance;
    }
    
    private PlayerModel _playerModel;
    
    public void Init(PlayerModel playerModel)
    {
        _playerModel = playerModel;
    }

    public int PlayerHiScore => _playerModel.hiScore;

    public int PlayerScore
    {
        get => _playerModel.score;
        set
        {
            _playerModel.score = value;
            if (_playerModel.score > _playerModel.hiScore)
            {
                _playerModel.hiScore = _playerModel.score;
                SavePlayerData();
            }
        }
    }

    public Color PlayerColor
    {
        get => _playerModel.color;
        set
        {
            _playerModel.color = value;
            SavePlayerData();
        }
    }
    
    public void SavePlayerData()
    {
        PlayerPrefsService.Get().SaveData(_playerModel, PlayerPrefsService.PlayerDataKey);
    }

    public void ResetPlayerScore()
    {
        _playerModel.score = 0;
    }
}
