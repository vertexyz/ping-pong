﻿using UnityEngine;

public class PlayerPrefsService
{
    private static readonly PlayerPrefsService _instance = new PlayerPrefsService();

    public static PlayerPrefsService Get()
    {
        return _instance;
    }
    
    public const string PlayerDataKey = "playerDataKey";

    public void SaveData(object data, string key)
    {
        PlayerPrefs.SetString(key, JsonUtility.ToJson(data));
    }
    
    public T LoadData<T>(string key)
    {
        var stringData = PlayerPrefs.GetString(key);
        var data = JsonUtility.FromJson<T>(stringData);
        return data;
    }
}
