﻿using System;
using UnityEngine;

public class AppMain : MonoBehaviour
{
    private static AppMain _instance;

    public static AppMain Get()
    {
        return _instance;
    }

    private string gameSettingsPath = "Settings/GameSettings";
    
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void InitializeOnLoad()
    {
        var go = new GameObject("AppMain");
        
        _instance = go.AddComponent<AppMain>();
        DontDestroyOnLoad(go);
        
        _instance.Init();
    }

    private void Init()
    {
        //load player model
        var loadedPlayerModel = PlayerPrefsService.Get().LoadData<PlayerModel>(PlayerPrefsService.PlayerDataKey);

        if (loadedPlayerModel == null)
        {
            loadedPlayerModel = new PlayerModel();
        }

        GameDataService.Get().Init(loadedPlayerModel);
        
        //load and init game settings
        GameSettings gameSettings = Resources.Load(gameSettingsPath, typeof(GameSettings)) as GameSettings;
        GameSettingsService.Get().Init(gameSettings);
        
        //Load Loader Scene
        if (SceneLoaderService.Get().GetCurrentScene() != ScenesEnum.Loader)
        {
            SceneLoaderService.Get().LoadScene(ScenesEnum.Loader);
        }
    }
}
