﻿using System;
using UnityEngine;

[Serializable]
public class PlayerModel
{
    public int score;
    public int hiScore;
    public Color color = Color.white;
}
