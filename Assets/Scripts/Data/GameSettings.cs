﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "Settings/Create Game Settings", order = 1)]
public class GameSettings : ScriptableObject
{
    [Header("Ball")]
    public float ballSpeed;
    public Vector2 ballSpeedRandomize;
    
    public float ballBounceOffsetFactor;
    public Vector2 ballBounceOffsetFactorRandomize;
    
    public float ballSize;
    public Vector2 ballSizeRandomize;
    
    [Header("Racket")]
    public int racketTranslateFactor;
    public int racketMoveMultiplier;

    [Header("Game")]
    public Vector2 horizontalBounds;
    public int scorePerRacketHit;
}