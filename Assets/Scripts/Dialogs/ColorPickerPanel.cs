﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPickerPanel : MonoBehaviour
{
    [SerializeField]
    private FlexibleColorPicker colorPicker;

    public Action<Color> OnColorChanged;
    
    public Action OnClose;

    private void Start()
    {
        colorPicker.OnColorChanged = OnColorChangedHandler;
    }

    private void OnColorChangedHandler(Color color)
    {
        OnColorChanged.Invoke(color);
    }

    public void OnCloseButtonClick()
    {
        OnClose?.Invoke();
    }

    private void OnDestroy()
    {
        colorPicker.OnColorChanged = null;
    }

    public void SetColor(Color color)
    {
        colorPicker.color = color;
    }
}
