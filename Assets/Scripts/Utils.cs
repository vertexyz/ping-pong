﻿using UnityEngine;

public static class Utils
{
    public static int GetRandomSign()
    {
        return Random.Range(0, 2) * 2 - 1;
    }

    public static float GetRandomRange(Vector2 range)
    {
        return Random.Range(range.x, range.y);
    }
}
